#include <iostream>

#include <config.h>
#include <myLib/myLib.h>

#include <fmt/core.h>


#define EXPECT_EQUAL(test, expect) equalityTest( test,  expect, \
                                                #test, #expect, \
                                                __FILE__, __LINE__)
template < typename T1, typename T2 >
int equalityTest(const T1          testValue,
                 const T2          expectedValue,
                 const std::string testName,
                 const std::string expectedName,
                 const std::string fileName,
                 const int         lineNumber)
{
    if (testValue != expectedValue)
    {
        fmt::print("{0}:{1}: Expected {2} to equal {4} ({5}) but it was {3}\n",
                   fileName,
                   lineNumber,
                   testName,
                   testValue,
                   expectedName,
                   expectedValue);

        return 1;
    }
    else
    {
        return 0;
    }
}


int main(int argc, char** argv)
{

    return EXPECT_EQUAL(getInt(), 41);
}