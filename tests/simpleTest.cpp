#include <iostream>
#include <string>

#include <config.h>
#include <myLib/myLib.h>

#include <fmt/core.h>

int main(int argc, char** argv)
{
    int returnValue = 0;

    // Add a check to make sure the major version doesn't change
    // as an example, but could be interesting for ensuring the version is not bumped unless the API changes
    if(VERSION_MAJOR != 0)
      returnValue = 1;

    // Check that a function returns an expected value
    if(getInt() != 42)
      returnValue = 1;

    // You could also add some output messages stating the problems for pinpointing the issues
    auto tmpStr = getMessage();
    if(tmpStr != "This is a message")
    {
      fmt::print("Expected \"This is a message\" from getMessage() but got \"{}\" instead.\n", tmpStr);
      returnValue = 1;
    }

    return returnValue;
}