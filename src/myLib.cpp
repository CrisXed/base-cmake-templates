#include <myLib/myLib.h>
#include <config.h>

#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include <iostream>

void printMessage()
{
    spdlog::trace("Entered myLib printMessage()");
    fmt::print("This is an fmt print\n");
    std::cout << "The Library also compiled and prints" << std::endl;
    std::cout << "  Version: " << VERSION_MAJOR << "." <<
                 VERSION_MINOR << "." << VERSION_PATCH << std::endl;
    std::cout << std::endl;

    return;
}

std::string getMessage()
{
    return "This is a message";
}

int getInt()
{
    return 42;
}

int addInt(int a, int b)
{
    return a+b;
}
