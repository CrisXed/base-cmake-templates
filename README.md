# Base CMake Templates

In this repository I am placing templates for my own personal use, using modern Cmake principles and C++.

The core idea is to have a main branch with a minimal configuration and alternative branches which use different libraries. For example, a branch which has WX Widgets pre configured.

All branches have a logging library configured by default.
My current preference for logging is spdlog (https://github.com/gabime/spdlog.git) and is the one currently used.
Together with spdlog, all branches have the fmt (https://github.com/fmtlib/fmt.git) library pre configured as well.
Unit testing libraries are not currently included by default, but are under consideration.

If anyone finds this repository useful I would be happy to hear about it. If there are any comments or suggestions on how to improve, I would be glad to hear any feedback, feel free to open an issue.

## Configuring SSH

Follow instructions from: https://docs.github.com/en/authentication/connecting-to-github-with-ssh
