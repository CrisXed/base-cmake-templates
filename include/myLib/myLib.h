#ifndef MY_LIBRARY_HEADER
#define MY_LIBRARY_HEADER

#include <string>

/// \brief Print a message to stdout
///
/// This function prints a message to the terminal, the message also contains version information
///
void printMessage();

/// \brief Get a message
///
/// This function retrieves a string with a message. The function is only used as an example
/// for the testing machinery
///
std::string getMessage();

/// \brief Get an int
///
/// This function retrieves an integer value. The function is only used as an example
/// for the testing machinery. This is probably how "Deep Thought" computed the answer
///
int getInt();

/// \brief Add two ints
///
/// This function takes two integer values and returns the sum
///
/// @param a the first integer
/// @param b the second integer
///
int addInt(int a, int b);

#endif