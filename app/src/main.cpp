#include <iostream>

#include <config.h>
#include <myLib/myLib.h>

#include <fmt/core.h>
#include <spdlog/spdlog.h>

int main(int argc, char** argv)
{
    #ifndef NDEBUG
    spdlog::warn("This is a debug build");
    SPDLOG_DEBUG("A different debug build message");
    #endif

    fmt::print("Application compiled and is running.\n");
    fmt::print("  Version: {}.{}.{}\n\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    fmt::print("Received {} parameters:\n", argc);
    for(int i = 0; i < argc; ++i)
      fmt::print(" Parameter[{}]: {}\n", i, argv[i]);
    fmt::print("\n");

    printMessage();

    fmt::print("A final message printed with {{fmt}}\n");

    return 0;
}